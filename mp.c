// This is free and unencumbered software released into the public domain.
// For more information, see LICENSE.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <memory.h>
#include <string.h>
#include <err.h>
#include <inttypes.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <pthread.h>
#include <stdatomic.h>

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))
#define clamp(p, a, b) (min(max(p, a), b))
#define cxsize(v) (sizeof(v) / sizeof(*v))
#define lerp(x, x0, y1, y0) (y0 + ((x - x0) * (y1 - y0)))
#define _str(a) #a
#define str(a) _str(a)
#define MAX_PERIOD (known_periods[0])
#define MIN_PERIOD (known_periods[cxsize(known_periods) - 1])
#define WAVE_SIZE  (64)
#define SAMP_RATE  48000

struct sample {
    char name[22];
    uint16_t size;
    int finetune;
    uint8_t volume;
    size_t repeat_point;
    size_t repeat_size;
    int8_t *data;
};

struct cell {
    uint16_t sample_idx, period;
    uint8_t op, x, y, xy;
};

struct song {
    char name[20];
    struct sample samples[31];
    uint8_t size;
    uint8_t positions[128];
    uint8_t *patterns;
};

struct file {
    uint8_t *data;
    size_t size;
};

static struct channel {
    float last_freq;
    uint16_t last_sample_idx;
    uint16_t last_period;
    int volume;
    int last_slide;
    int last_vibrato;
    int last_tremolo;
    int tremolo_wave;
    int vibrato_wave;
    int repeat;
} channels[8];

static size_t nchannels;
static atomic_int divsize = 0x400;
static atomic_int speed = 6 * 0x400;
static atomic_int finetune = 0;
static _Atomic(float) glob_vol = 1 / 4.;

char *notes[] = {
     "C", "C#",  "D", "D#",  "E",  "F", "F#",  "G", "G#",  "A", "A#",  "B",
};

int known_periods[] = {
    1712, 1616, 1525, 1440, 1357, 1281, 1209, 1141, 1077, 1017,  961,  907,
     856,  808,  762,  720,  678,  640,  604,  570,  538,  508,  480,  453,
     428,  404,  381,  360,  339,  320,  302,  285,  269,  254,  240,  226,
     214,  202,  190,  180,  170,  160,  151,  143,  135,  127,  120,  113,
     107,  101,   95,   90,   85,   80,   76,   71,   67,   64,   60,   57,
};

static float waves[4][WAVE_SIZE];

static uint16_t     add_semitones(uint16_t, int);
static uint16_t     sub_semitones(uint16_t, int);
static struct cell  read_cell(uint8_t *);
static size_t       known_perdiod_idx(uint16_t);
static int8_t       sample_at(struct sample *, float);
static void         channel_play(struct channel *, struct song *, struct cell,
    float *, size_t);
static int          read_file(const char *, struct file *);
static void         print_line(uint8_t *);
static int          song_read(struct song *, uint8_t *, size_t );
static void         init_waves(void *);
static int          song_play(struct song *);

int
main(int argc, char *argv[]) {
    struct file file;
    struct song song;
    struct termios curr, old;
    void restore_term(void) { tcsetattr(STDIN_FILENO,TCSANOW, &old); }
    void * th_play(void *arg) {
        if (song_play(arg) != 0) err(1, "song_play");
        return NULL;
    }
    pthread_t th;
    unsigned char c;

    if (argc - 1 < 1) errx(1, "not enough arguments");
    if (read_file(argv[1], &file) != 0) err(1, "read_file");
    if (song_read(&song, file.data, file.size) != 0) errx(1, "malformed file");
    if (!isatty(STDIN_FILENO) && song_play(&song) != 0) err(1, "song_play"); 
    if (tcgetattr(STDIN_FILENO, &old) == -1) err(1, "tcgetattr");
    if (atexit(restore_term) != 0) err(1, "atexit");

    curr = old;
    curr.c_lflag &=(~ICANON & ~ECHO);

    if (tcsetattr(STDIN_FILENO,TCSANOW, &curr) == -1) err(1, "tcsetattr");
    if (pthread_create(&th, NULL, th_play, &song) != 0) {
        err(1, "pthread_create");
    }
    while ((c = getchar()) != 'q') {
        switch(c){
        case 'a': if (divsize > 0x20) {
            speed /= divsize;
            divsize -= 0x20;
            speed *= divsize;
        } break;
        case 's': if (divsize < 0x800) {
            speed /= divsize;
            divsize += 0x20;
            speed *= divsize;
        } break;
        case 'z': finetune++; break;
        case 'x': finetune--; break;
        case 'w': if (glob_vol < 0.5f)   glob_vol = glob_vol + 0.005f; break;
        case 'e': if (glob_vol > 0.05f) glob_vol = glob_vol - 0.005f; break;
        case 'A': if (divsize > 0x100) {
            speed /= divsize;
            divsize -= 0x100;
            speed *= divsize;
        } break;
        case 'S': if (divsize < 0x800) {
            speed /= divsize;
            divsize += 0x100;
            speed *= divsize;
        } break;
        case 'Z': finetune += 0x10; break;
        case 'X': finetune -= 0x10; break;
        case 'W': if (glob_vol < 0.5f)   glob_vol = glob_vol + 0.005f; break;
        case 'E': if (glob_vol > 0.05f) glob_vol = glob_vol - 0.05f; break;
        }
    }
    return 0;
}

static uint16_t
add_semitones(uint16_t period, int nsemi) {
    for (int i = 0; i < nsemi; i++) {
        period = floor(period * 0.94412);
    }
    return period;
}

static uint16_t
sub_semitones(uint16_t period, int nsemi) {
    for (int i = 0; i < nsemi; i++) {
        period = ceil(period * 1.0592);
    }
    return period;
}

static struct cell
read_cell(uint8_t *s) {
    unsigned sample_idx = ((s[0] & 0xf0)     ) | (s[2] >> 4);
    unsigned period     = ((s[0] & 0x0f) << 8) | (s[1]     );
    unsigned command    = ((s[2] & 0x0f) << 8) | (s[3]     );

    return (struct cell){
        .sample_idx = sample_idx,
        .period     = period,
        .op = (command & 0xf00) >> 8,
        .xy = (command & 0x0ff) >> 0,
        .x  = (command & 0x0f0) >> 4,
        .y  = (command & 0x00f) >> 0,
    };
}

static size_t
known_perdiod_idx(uint16_t period) {
    size_t i = 0;

    while (i < cxsize(known_periods) && known_periods[i] > period) {
        i++;
    }
    return i < cxsize(known_periods) ? i : i - 1;
}

static int8_t
sample_at(struct sample *sample, float f) {
    size_t j = f;
    return sample->data[j];
}

static void
channel_play(struct channel *ch, struct song *song, struct cell c, float *buf,
    size_t size) {
    struct sample *sample;
    float period = c.period;
    uint16_t sample_idx = c.sample_idx;
    int op = c.op, xy = c.xy, x = c.x, y = c.y;
    int slide = 0, vibrato = 0, vibrato_pos = 0, tremolo = 0, tremolo_pos = 0;
    size_t nticks = size / divsize, i = 0;
    float f = 0, dp = 0, div = divsize, freq, volume, ftune;
    uint16_t slide_to;

    void process_sample(void) {
        int tick = i / divsize;
        float *wave;
        buf[i] += volume * sample_at(sample, f);
        if (i % divsize == 0) {
            switch (op) {
            case 0:  if (xy == 0) break;
                else if (tick % 3 == 0) period = period;
                else if (tick % 3 == 1) period = add_semitones(period, x);
                else if (tick % 3 == 2) period = add_semitones(period, y);
            break;
            case 0x5: case 0x6: case 0xa: ch->volume += x ? x : -y; break;
            case 0x7:
                x = tremolo >> 4, y = tremolo & 0xf;
                wave = waves[ch->tremolo_wave];
                ch->volume += 16. * wave[tremolo_pos] * y / nchannels;
                tremolo_pos = (tremolo_pos + x * nticks) % WAVE_SIZE;
                break;
            case 0xc: ch->volume = xy; break;
            }
            ch->volume = clamp(ch->volume, 0, 64);
            volume = ch->volume * glob_vol / nchannels;
            if (op == 0x4 || op == 0x6) {
                x = vibrato >> 4, y = vibrato & 0xf;
                wave = waves[ch->vibrato_wave];
                dp = wave[vibrato_pos] * y;
                vibrato_pos = (vibrato_pos + x * nticks) % WAVE_SIZE;
            }
        }
        if (tick == 0) {
            switch (op) {
            case 0xE:
                if (x == 1) period = max(MIN_PERIOD, period - y / div);
                if (x == 2) period = min(MAX_PERIOD, period + y / div);
                break;
            }
        } else {
            switch (op) {
            case 0x1: period = max(MIN_PERIOD, period - xy / div); break;
            case 0x2: period = min(MAX_PERIOD, period + xy / div); break;
            case 0x3: case 0x5:
                if (slide_to - period) {
                    period = min(slide_to, period + slide / div);
                } else {
                    period = max(slide_to, period - slide / div);
                } 
                break;
            case 0x4: case 0x6: period += dp / div; break;
            }
        }
    }
    if ((period == 0 && ch->last_period == 0) ||
        (sample_idx == 0 && ch->last_sample_idx == 0)) {
        return;
    }
    if (period == 0) {
        period = ch->last_period;
    }
    switch (op) {
    case 0x3:
        slide = xy ? xy : ch->last_slide;
        slide_to = period;
        period = ch->last_period;
        break;
    case 0x5: slide = ch->last_slide; break;
    case 0x4: vibrato = xy ? xy : ch->last_vibrato; break;
    case 0x7: tremolo = xy ? xy : ch->last_tremolo; break;
    case 0x6: vibrato = ch->last_vibrato; break;
    case 0x9: f = min(xy * 0x200ull, size); break;
    }
    if (sample_idx != 0) {
        sample = song->samples + sample_idx - 1;
        ftune = pow(1.007246412, -sample->finetune + finetune);
        freq = 3546894.6 / period * ftune / SAMP_RATE;
        ch->volume = sample->volume;
        volume = ch->volume * glob_vol / nchannels;
        ch->repeat = 0;
        for (; i < size && f < sample->size; i++, f += freq) {
            process_sample();
        }
    } else {
        sample = song->samples + ch->last_sample_idx - 1;
        f = ch->last_freq;
        ftune = pow(1.007246412, -sample->finetune + finetune);
        freq = 3546894.6 / period * ftune / SAMP_RATE;
        volume = ch->volume * glob_vol / nchannels;
        sample_idx = ch->last_sample_idx;
    }
    for (; i < size && sample->repeat_size; i++, f += freq) {
        if (f >= sample->size) {
            ch->repeat = 1;
        }
        if (ch->repeat &&
            (f >= sample->repeat_point + sample->repeat_size ||
             f >= sample->size)) {
            f = sample->repeat_point;
        }
        process_sample();
    }
    if (slide) ch->last_slide = slide;
    if (vibrato) ch->last_vibrato = vibrato;
    ch->last_sample_idx = sample_idx;
    ch->last_period = period;
    ch->last_freq = f;
}

static int
read_file(const char *fname, struct file *file) {
    int fd = open(fname, O_RDONLY);
    off_t size;

    if (fd == -1) {
        return -1;
    }
    size = lseek(fd, 0, SEEK_END);
    if (size == -1 || lseek(fd, 0, SEEK_SET) == -1) {
        close(fd);
        return -1;
    }
    file->size = size;
    file->data = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
    close(fd);

    if (file->data == MAP_FAILED) {
        return -1;
    }
    return 0;
}

static void
print_line(uint8_t *line) {
    for (size_t i = 0; i < nchannels; i++) {
        struct cell c = read_cell(line + i * 4);
        char rep_sample[3] =   " -";
        char rep_period[5] = "  - ";
        char rep_command[4] = " - ";

        if (c.sample_idx) {
            snprintf(rep_sample, 3, "%2d", c.sample_idx);
        }
        if (c.period) {
            int note_idx = known_perdiod_idx(c.period);
            int octave = note_idx / cxsize(notes);
            char *note = notes[note_idx % cxsize(notes)];
            snprintf(rep_period, 5, " %2s%d", note, octave);
        }
        if (c.op || c.xy) {
            snprintf(rep_command, 4, "%01X%02X", c.op, c.xy);
        }
        printf("| %s %s %s ", rep_sample, rep_period, rep_command);
    }
    printf("|\n");
}

static int
song_read(struct song *song, uint8_t *data, size_t size) {
    size_t required_size = 1084;
    uint8_t max_pattern = 0;
    uint8_t *tag = data + 1080;

    if (size < required_size) {
        return -1;
    }
    memcpy(song->name, data, 19);
    song->name[19] = 0;

    for (int i = 0; i < 31; i++) {
        uint8_t *name_off = data + 30 * i + 20;
        uint8_t *size_off = data + 30 * i + 42;
        uint8_t *finetune_off = data + 30 * i + 44;
        uint8_t *volume_off = data + 30 * i + 45;
        uint8_t *rep_point_off = data + 30 * i + 46;
        uint8_t *rep_size_off = data + 30 * i + 48;

        song->samples[i] = (struct sample){
            .size = 2 * ((size_off[0] << 8) | (size_off[1])),
            .repeat_point = 2 * ((rep_point_off[0] << 8) | (rep_point_off[1])),
            .repeat_size = 2 * ((rep_size_off[0] << 8) | (rep_size_off[1])),
            .volume = min(*volume_off, 64),
            .finetune = (*finetune_off & 0x07) - (*finetune_off & 0x8),
        };
        memcpy(song->samples[i].name, name_off, 21);
        song->samples[i].name[21] = 0;
    }
    song->size = clamp(data[950], 1, 128);
    memcpy(song->positions, data + 952, 128);

    if (memcmp(tag, "M.K.", 4) == 0) {
        nchannels = 4;
    } else if (memcmp(tag, "6CHN", 4) == 0) {
        nchannels = 6;
    } else if (memcmp(tag, "8CHN", 4) == 0) {
        nchannels = 8;
    } else {
        return -1;
    }
    for (int i = 0; i < 128; i++) {
        max_pattern = max(song->positions[i], max_pattern);
    }
    song->patterns = data + required_size;
    required_size += 256 * nchannels * (max_pattern + 1);

    for (uint8_t *sample_data = data + required_size, i = 0; i < 31; i++) {
        song->samples[i].data = (int8_t *)sample_data;
        sample_data += song->samples[i].size;
        required_size += song->samples[i].size;
    }
    if (size < required_size) {
        return -1;
    }
    return 0;
}

static void
init_waves(void *buf) {
    float *waves = buf;

    for (size_t i = 0; i < WAVE_SIZE; i++) {
        waves[0 * WAVE_SIZE + i] = (sin(i * 2 * M_PI / WAVE_SIZE));
    }
    for (size_t i = 0; i < WAVE_SIZE; i++) {
        waves[1 * WAVE_SIZE + i] = (1 - 2.  * i / WAVE_SIZE);
    }
    for (size_t i = 0; i < WAVE_SIZE; i++) {
        waves[2 * WAVE_SIZE + i] = (i < WAVE_SIZE / 2 ? 1 : -1);
    }
    for (size_t i = 0, nx = 1234; i < WAVE_SIZE; i++) {
        nx = nx * 1103515245 + 12345;
        waves[3 * WAVE_SIZE + i] =
            (((unsigned)(nx/65536) % 32768) - 16383.5) / 32767.;
    }
}

static int
song_play(struct song *song) {
    static float buf[0x800 * 0xff] = { 0 };
    static int16_t ibuf[0x800 * 0xff] = { 0 };
    float s0 = 0, s1 = 0, s2 = 0;
    init_waves(waves);
    size_t i = 0, k = 0, jmp = 0;
    uint8_t *line;
    FILE *sox = popen("play -t s16 -b 16 -c1 -v4 -r " str(SAMP_RATE)
        " - 2>/dev/null", "w"
    );
    if (sox == NULL) {
        return -1;
    }
    while (k < song->size) {
        i = 0;
        while (i < 64) {
            line = song->patterns + (song->positions[k]*64+i) * 4 * nchannels;
            print_line(line);
            for (size_t j = 0; j < nchannels; j++) {
                struct cell c = read_cell(line + 4 * j);
                if (c.op == 0xf) {
                    if (c.xy < 0x20) speed = c.xy * divsize;
                    else speed = 750 * divsize / c.xy;
                }
                channel_play(channels + j, song, c, buf, speed);
                if (c.op == 0xb) {
                    if (c.xy < song->size) {
                        k = c.xy, jmp = 1;
                    } else {
                        goto SONG_END;
                    }
                } else if (c.op == 0xd) {
                    k++, i = c.x * 10 + c.y, jmp = 1;
                    break;
                }
            }
            for (int i = 0; i < speed; i++) {
                ibuf[i] = (buf[i] + s0 + s1 + s2) / 4.;
                s2 = s1;
                s1 = s0;
                s0 = buf[i];
            }
            fwrite(ibuf, speed, sizeof(*ibuf), sox);
            memset(buf, 0, sizeof(buf));
            if (jmp) {
                jmp = 0;
                continue;
            }
            i++;
        }
        k++;
    }
SONG_END:
    pclose(sox);
    return 0;
}

