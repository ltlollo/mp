# mp

an amiga mod player

## requirements
* gcc
* sox

## compiling

```
make mp
```

## notes

* 6-8 channels files not tested/working
* some extended commands not implemented
